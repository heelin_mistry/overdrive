import random

import gym
import numpy as np
from gym import spaces


class OverdriveEnv(gym.Env):

    def __init__(self, is_training=True):
        self.trainingMode = is_training

        self.observation_space = tuple([1, 208])
        self.action_space = spaces.Discrete(7)

        self.playerA = [0, 0]

        self.speed = 5
        self.power_ups = []
        self.boosting = False
        self.boost_counter = 0

        self.map = []
        self.lanes = 4
        self.game_length = 1500
        self.previous_tile = 0

        self.current_step = 0

    def step(self, action):
        speed, reward = self.take_action(action)
        done = False

        self.current_step += 1
        next_reward, next_state = self.get_next_state(speed)
        normal_end = self.playerA[1] == self.game_length-1

        reward += next_reward

        ratio = self.current_step / self.game_length
        if normal_end:
            done = True
            reward = np.round(2000 * (1-ratio))

        if self.current_step > 700:
            done = True
            reward = -100

            # reward = (self.game_length - self.current_step) / self.lanes
            # if self.current_step > 600:
            #     reward = -10
            # elif self.current_step > 500:
            #     reward += 0
            # elif self.current_step > 400:
            #     reward += 100
            # elif self.current_step > 300:
            #     reward += 200
            # elif self.current_step > 200:
            #     reward += 400
            # elif self.current_step > 100:
            #     reward += 600
            # else:
            #     reward += 800

        return next_state, reward, done, {}

    def take_action(self, action):
        # 0 = Nothing
        # 1 = Accelerate
        # 2 = Decelerate
        # 3 = Turn Left
        # 4 = Turn Right
        # 5 = Use Boost
        # 6 = Use Oil
        reward = -1
        current_speed = self.speed

        if action == 0:
            reward += 0
        elif action == 1:
            if self.speed >= 9:
                reward -= 4
            self.adjust_speed(action)
            current_speed = self.speed
        elif action == 2:
            self.adjust_speed(action)
            current_speed = self.speed
            if current_speed <= 0:
                reward -= 4
        elif action == 3:
            if self.playerA[0] > 0 and self.speed > 0:
                self.map[self.playerA[0]][self.playerA[1]] = self.previous_tile
                self.playerA[0] = self.playerA[0] - 1
                self.previous_tile = self.map[self.playerA[0]][self.playerA[1]]
                current_speed -= 1
            else:
                reward -= 4
        elif action == 4 and self.speed > 0:
            if self.playerA[0] < self.lanes-1:
                self.map[self.playerA[0]][self.playerA[1]] = self.previous_tile
                self.playerA[0] = self.playerA[0] + 1
                self.previous_tile = self.map[self.playerA[0]][self.playerA[1]]
                current_speed -= 1
            else:
                reward -= 4
        elif action == 5:
            if 4 in self.power_ups and not self.boosting:
                self.power_ups.remove(4)
                self.boosting = True
                self.speed = 15
                current_speed = self.speed
            else:
                reward -= 4
        elif action == 6:
            if 5 in self.power_ups:
                self.power_ups.remove(5)
                current_speed = self.speed
            else:
                reward -= 0
        return current_speed, reward

    def reset(self):
        self.current_step = 0
        self.map = []
        self.playerA = [0, 0]
        self.speed = 5
        self.power_ups = []
        self.boost_counter = 0
        self.boosting = False
        self.previous_tile = 0
        for lane_index in range(0, self.lanes):
            lane = [0] * self.game_length
            # 0 - Empty
            # 1 - Player 1
            # 2 - Mud
            # 3 - Oil Spill
            # 4 - Boost
            # 5 - Oil Item
            # 6 - Finish
            lane[-1] = 6
            # no_obsticles = self.game_length * (random.randint(4, 6) / 10)
            no_obsticles = self.game_length * 0.2
            obsticles_index = []
            obsticles_index.append(0)
            obsticles_index.append(1)
            obsticles_index.append(2)
            obsticles_index.append(3)
            obsticles_index.append(self.game_length-1)
            if lane_index == self.playerA[0] and not obsticles_index.__contains__(self.playerA[1]):
                obsticles_index.append(self.playerA[1])
            while obsticles_index.__len__() < no_obsticles:
                index = random.randint(0, self.game_length-1)
                if index not in obsticles_index:
                    lane[index] = random.randint(2, 5)
                    obsticles_index.append(index)
            self.map.append(lane)

        reward, new_state = self.get_next_state(0)
        return new_state

    def get_next_state(self, speed):
        # 5 behind
        # 20 ahead
        future = 21
        past = 5
        current_index = self.playerA[1]

        if self.boosting and self.boost_counter < 5:
            self.boost_counter += 1
        else:
            self.boosting = False
            self.boost_counter = 0

        self.map[self.playerA[0]][self.playerA[1]] = self.previous_tile
        if self.playerA[1] + speed < self.game_length:
            self.playerA[1] += speed
        else:
            self.playerA[1] = self.game_length-1
        self.previous_tile = self.map[self.playerA[0]][self.playerA[1]]
        self.map[self.playerA[0]][self.playerA[1]] = 1

        full_movement = []
        for lane in range(0, self.lanes):
            movement = self.map[lane][current_index:self.playerA[1]]
            if movement.__len__() > 0:
                full_movement.append(movement)
        reward, movement = self.sort_movement(full_movement)
        del movement, full_movement

        current_index = self.playerA[1]
        future_vision = []
        for index in range(0, self.lanes):
            lane = []
            if current_index == 0:
                vision = ([0] * past) + self.map[index][current_index:future]
                lane.extend(vision)
            elif current_index < past:
                vision = ([0] * (past - current_index)) + self.map[index][0:current_index+future]
                lane.extend(vision)
            elif current_index > self.game_length - future:
                vision = self.map[index][current_index-past:self.game_length] + ([0] * (future - (self.game_length-current_index)))
                lane.extend(vision)
            else:
                lane.extend(self.map[index][current_index-past:current_index+future])
            future_vision.append(np.array(lane))
        del current_index

        boost_lane = [0] * future_vision[0].__len__()
        if self.boosting:
            boost_lane[0] = 5
        future_vision.append(np.array(boost_lane))
        # Add speed
        speed_lane = [0] * future_vision[0].__len__()
        for speed in range(0, min(self.speed, future_vision[0].__len__())):
            speed_lane[speed] = 1
        future_vision.append(np.array(speed_lane))
        del speed_lane, boost_lane

        # Add powerup
        power_lane = [0] * future_vision[0].__len__()
        boost = self.power_ups.count(4)
        for power in range(0, min(boost, future_vision[0].__len__())):
            power_lane[power] = 1
        future_vision.append(np.array(power_lane))
        boost = self.power_ups.count(5)
        for power in range(0, min(boost, future_vision[0].__len__())):
            power_lane[power] = 1
        future_vision.append(np.array(power_lane))
        del power_lane, boost

        return reward, np.array(future_vision)

    def sort_movement(self, full_movement):
        reward = 0
        if full_movement.__len__() == 0:
            return reward, full_movement

        # current_lane = self.playerA[0]
        # if current_lane == 0 and self.lanes > current_lane:
        #     lane = full_movement[current_lane+1]
        #     for index in range(0, lane.__len__()):
        #         if lane[index] == 2 or lane[index] == 3:
        #             reward += 1
        # elif 0 < current_lane < self.lanes - 1:
        #     prev_lane = full_movement[current_lane-1]
        #     next_lane = full_movement[current_lane+1]
        #     for index in range(0, prev_lane.__len__()):
        #         if prev_lane[index] == 2 or prev_lane[index] == 3:
        #             reward += 2
        #     for index in range(0, next_lane.__len__()):
        #         if next_lane[index] == 2 or next_lane[index] == 3:
        #             reward += 2
        # elif current_lane == self.lanes-1:
        #     prev_lane = full_movement[current_lane-1]
        #     for index in range(0, prev_lane.__len__()):
        #         if prev_lane[index] == 2 or prev_lane[index] == 3:
        #             reward += 2

        movement = full_movement[self.playerA[0]]
        valid = movement

        mud = valid.count(2)
        oil_spill = valid.count(3)
        boost = valid.count(4)
        oil_item = valid.count(5)

        if mud != 0 or oil_spill != 0:
            self.adjust_speed(2)
            reward -= 4

        for _ in range(mud):
            self.boosting = False
            self.boost_counter = 0
            reward -= 3

        for _ in range(oil_spill):
            self.boosting = False
            self.boost_counter = 0
            reward -= 4

        for index in range(boost):
            self.power_ups.append(4)
            reward += 4

        for index in range(oil_item):
            self.power_ups.append(5)
            reward += 4

        return reward, movement

    def adjust_speed(self, action):
        current_speed = self.speed
        if action == 1:
            if current_speed < 3:               # Min Speed
                self.speed = 3
            elif current_speed < 6:            # State 1
                self.speed = 6
            elif current_speed < 8:            # State 2
                self.speed = 8
            elif current_speed < 9:            # State 3
                self.speed = 9
        elif action == 2:
            if current_speed > 9:            # Boost Speed
                self.speed = 9
            elif current_speed > 8:            # Max Speed
                self.speed = 8
            elif current_speed > 6:            # State 3
                self.speed = 6
            elif current_speed > 3:            # State 2
                self.speed = 3
            elif current_speed > 0:            # State 1
                self.speed = 0
