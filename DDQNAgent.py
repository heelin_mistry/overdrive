import os
import random

import numpy as np
from keras import Sequential
from keras.constraints import maxnorm
from keras.layers import Dense, Dropout
from keras.models import model_from_json
from keras.optimizers import Adam

GAMMA = 0.99
MEMORY_SIZE = 10000
BATCH_SIZE = 64
# BATCH_SIZE = 100000
TRAINING_FREQUENCY = 100
# TARGET_NETWORK_UPDATE_FREQUENCY = 20000
TARGET_NETWORK_UPDATE_FREQUENCY = 5000
REPLAY_START_SIZE = 200

EXPLORATION_MAX = 1.0
EXPLORATION_MIN = 0.1
EXPLORATION_TEST = 0.02
EXPLORATION_STEPS = 500000
EXPLORATION_DECAY = (EXPLORATION_MAX - EXPLORATION_MIN) / EXPLORATION_STEPS


class DDQN:

    def __init__(self, action_space, training=True):
        self.loss_rate = None
        self.acc_rate = None
        self.action_space = action_space
        self.training = training
        self.network_update_step = 0
        self.model = None

        if training:
            model = Sequential()
            # model.add(Dense(64, input_dim=208, activation='relu'))
            # model.add(Dense(self.action_space, activation='softmax'))
            # model.compile(loss='mse', optimizer='adam')

            model.add(Dense(128, input_dim=208, activation='elu'))
            # model.add(BatchNormalization())       change use_bias to false in previous layer
            # model.add(Dropout(0.5))
            model.add(Dense(64, activation='elu'))
            # model.add(BatchNormalization())
            # model.add(Dropout(0.5))
            model.add(Dense(32, activation='elu'))
            # model.add(BatchNormalization())
            # model.add(Dropout(0.5))
            model.add(Dense(self.action_space, activation='softmax'))
            # model.add(Dropout(0.5))

            # model = Sequential()
            # model.add(Dense(units=64, input_dim=208, activation='relu'))
            # # model.add(Dropout(0.5))
            # model.add(Dense(units=42, activation='relu'))
            # # model.add(Dropout(0.5))
            # model.add(Dense(output_dim=self.action_space, activation='softmax'))
            model.compile(loss='mean_squared_error', optimizer='adam', metrics=['accuracy'])

            self.model = model.model
            self.ddqn_target = model.model
            self._reset_target_network()
        else:
            self.model = self.load_models_from_file()
            # self.model.compile(optimizer='adam', loss='mse')
            self.model.compile(loss='mse', optimizer=Adam(lr=0.01))
            self.ddqn_target = self.model.model
            self._reset_target_network()

        self.epsilon = EXPLORATION_MAX
        self.memory = []

    def act(self, state):
        if self.training:
            if np.random.rand() < self.epsilon or len(self.memory) < REPLAY_START_SIZE:
                return random.randrange(self.action_space)
            q_values = self.model.predict(state)
            return np.argmax(q_values[0])
        else:
            if np.random.rand() < EXPLORATION_TEST:
                return random.randrange(self.action_space)
            q_values = self.model.predict(state)
            return np.argmax(q_values[0])

    def remember(self, current_state, action, reward, next_state, terminal):
        self.memory.append((current_state, action, reward, next_state, terminal))

        if len(self.memory) > MEMORY_SIZE:
            self.memory.pop(0)

    def step_update(self):
        self.network_update_step += 1
        if len(self.memory) < REPLAY_START_SIZE:
            return

        if self.network_update_step % TRAINING_FREQUENCY == 0:
            self._train()

        self._update_epsilon()

        if self.network_update_step % TARGET_NETWORK_UPDATE_FREQUENCY == 0:
            self._reset_target_network()

    def _train(self):
        batch = random.sample(self.memory, BATCH_SIZE)
        if len(batch) < BATCH_SIZE:
            return

        current_states = []
        q_values = []
        max_q_values = []

        for current_state, action, reward, next_state, terminal in batch:
            current_state = current_state
            current_states.append(current_state)
            next_state = next_state
            next_state_prediction = self.ddqn_target.predict(next_state).ravel()
            next_q_value = np.max(next_state_prediction)
            q = list(self.model.predict(current_state)[0])
            if terminal:
                q[action] = reward
            else:
                q[action] = reward + GAMMA * next_q_value
            q_values.append(q)
            max_q_values.append(np.max(q))

        history = self.model.fit(np.asarray(current_states).squeeze(),
                                   np.asarray(q_values).squeeze(),
                                   batch_size=BATCH_SIZE,
                                   verbose=0)


    def _update_epsilon(self):
        self.epsilon -= EXPLORATION_DECAY
        self.epsilon = max(EXPLORATION_MIN, self.epsilon)

    def _reset_target_network(self):
        self.ddqn_target.set_weights(self.model.get_weights())

    @staticmethod
    def save_model(agent):
        cwd = os.getcwd()

        model_json = agent.model.to_json()
        with open(cwd + "/Overdrive_Model.json", "w") as json_file:
            json_file.write(model_json)

        agent.model.save_weights(cwd + "/Overdrive_Weights.h5")
        print("Saved model to disk")

    @staticmethod
    def load_models_from_file():
        cwd = os.getcwd()

        json_file = open(cwd + "/Overdrive_Model.json", "r")
        loaded_model_json = json_file.read()
        json_file.close()

        loaded_model = model_from_json(loaded_model_json)
        loaded_model.load_weights(cwd + "/Overdrive_Weights.h5")
        print("Loaded model from disk")

        return loaded_model