import pandas as pd
import numpy as np

pd.set_option('display.width', 1000)
pd.set_option('display.max_columns', 100)

from OverdriveEnv import OverdriveEnv
from DDQNAgent import DDQN

log_interval = 100
save_interval = 30000

env = OverdriveEnv()
observation_space = env.observation_space
action_space = env.action_space.n

agent = DDQN(action_space, training=True)

episode = 1
best_round = 1
best_round_reward = 0.0
average = 0
while True:
    rounds = 0
    total_round_reward = 0
    state = env.reset()
    state = np.reshape(state, [1, 208])
    done = False
    while not done:
        rounds += 1
        action = agent.act(state)
        next_state, reward, done, info = env.step(action)
        next_state = np.reshape(next_state, [1, 208])
        total_round_reward += reward
        agent.remember(state, action, reward, next_state, done)
        state = next_state
        agent.step_update()

    # average += total_round_reward
    step_ratio = env.current_step/env.game_length
    if best_round_reward <= total_round_reward:
        best_round = step_ratio
        # if best_round_reward < total_round_reward:
        best_round_reward = total_round_reward
        print("Saving Rounds: {}\tGame Length: {},\tReward: {}".format(env.current_step, env.game_length, best_round_reward))
        agent.save_model(agent)
    print("Episode: {}, Epsilon: {}, Rounds: {},\tReward: {}".format(episode, agent.epsilon, rounds, total_round_reward))

    episode += 1
